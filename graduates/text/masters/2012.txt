Андреенков Евгений Сергеевич – Электроэнергетика
Баранов Андрей Михайлович – Электроника и микроэлектроника
Волосенков Алексей Владимирович – Информатика и вычислительная техника
Герасимов Максим Валерьевич – Теплоэнергетика
Данилов Алексей Владимирович – Теплоэнергетика
Ерохин Дмитрий Владимирович – Информатика и вычислительная техника
Захаров Андрей Сергеевич – Информатика и вычислительная техника
Калитиненков Максим Сергеевич – Информатика и вычислительная техника
Кузютина Анна Владимировна – Электроника и микроэлектроника
Куликова Ольга Андреевна – Информатика и вычислительная техника
Липовой Виктор Геннадьевич – Электроника и микроэлектроника
Маркелова Лидия Павловна – Электроэнергетика
Пилюшин Михаил Валерьевич – Электротехника, электромеханика и электронные технологии
Писаренко Антон Сергеевич – Электроэнергетика
Савельев Никита Владимирович – Электротехника, электромеханика и электронные технологии
Скорубский Вацлав Евгеньевич – Электроэнергетика
Соловьев Игорь Владимирович – Электроэнергетика
Солопов Артем Олегович – Электроэнергетика
Станьков Дмитрий Александрович – Электроэнергетика
Шишея Алексей Валерьевич – Электроэнергетика
Штемпель Алексей Николаевич – Электроника и микроэлектроника
Шукаев Сергей Анатольевич – Электроэнергетика
Якуненков Сергей Юрьевич – Электроэнергетика
