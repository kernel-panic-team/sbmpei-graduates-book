import os
from time import time

from graduates.classes import Templates, Content
from graduates.utils import extract_entries, split_to_pages, generate_names_mapping, generate_main_js, get_page_class, \
    process_pages, specialist_subtext_matcher

ENTRIES_PER_PAGE = 30
ENTRIES_MIN_BEFORE_END = 2

PAGE_TITLE = 'Дипломы с отличием получили'

TITLE = 'Дипломы с отличием получили'
SUBTITLE = 'филиал ФГБОУ ВО «Национальный исследовательский университет «МЭИ» в г.&nbsp;Смоленске'

SPEC_DIR = 'graduates/text/specialists'
BACH_DIR = 'graduates/text/bachelors'
MAST_DIR = 'graduates/text/masters'

BUILD_DIR = os.getcwd() + '/build/'

t = Templates()

p_num = 1

content = Content()

content.append(t.first.render(page_class=get_page_class(p_num)))
p_num += 1

content.append(t.first.render(page_class=get_page_class(p_num)))
p_num += 1

content.append(t.front.render(title=TITLE, subtitle=SUBTITLE, page_class=get_page_class(p_num)))
p_num += 1

# used for year to page mapping
s_years = []
b_years = []
m_years = []

# used for search
search_data = list()

# parse all entries
s_pages = split_to_pages(extract_entries(SPEC_DIR, subtext_matcher=specialist_subtext_matcher), ENTRIES_PER_PAGE, ENTRIES_MIN_BEFORE_END)
b_pages = split_to_pages(extract_entries(BACH_DIR), ENTRIES_PER_PAGE, ENTRIES_MIN_BEFORE_END)
m_pages = split_to_pages(extract_entries(MAST_DIR), ENTRIES_PER_PAGE, ENTRIES_MIN_BEFORE_END)

p_num = process_pages(s_pages, content.append, search_data, s_years, p_num, t)

content.append(t.sep.render(title='БАКАЛАВРЫ', page_class=get_page_class(p_num)))
p_num += 1

p_num = process_pages(b_pages, content.append, search_data, b_years, p_num, t)

content.append(t.sep.render(title='МАГИСТРЫ', page_class=get_page_class(p_num)))
p_num += 1

p_num = process_pages(m_pages, content.append, search_data, m_years, p_num, t)

if p_num % 2 == 1:
    content.append(t.page.render(entries=[], page_class=get_page_class(p_num)))

info = dict()
info['short_sha'] = os.getenv('CI_COMMIT_SHORT_SHA', '')

timestamp = str(round(time()))
js_file_name = 'main-' + timestamp + '.js'
nm_file_name = 'nm-' + timestamp + '.json'

html = t.index.render(
    js_file_name=js_file_name,
    page_title=PAGE_TITLE,
    content=str(content),
    s_years=s_years,
    b_years=b_years,
    m_years=m_years,
    info=info)

with open(BUILD_DIR + 'index.html', 'w', encoding='utf8') as f:
    f.write(html)

with open(BUILD_DIR + nm_file_name, 'w', encoding='utf8') as f:
    generate_names_mapping(search_data, f)

with open(BUILD_DIR + js_file_name, 'w', encoding='utf8') as f:
    generate_main_js(nm_file_name, f)

