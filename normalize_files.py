#!/bin/python

import os
import re

root = 'graduates/text/'
dirs = [
    root + 'bachelors',
    root + 'masters',
    root + 'specialists'
]

file_regex = re.compile('^\d{4}\.(?i:txt)$')

for d in dirs:
    files = [d + '/' + file for file in os.listdir(d) if file_regex.match(file)]
    for f in files:

        old_file = f
        new_file = f.lower()

        if old_file != new_file:
            os.renames(old_file, new_file)
            os.system('git add ' + new_file)
            print('{} -> {}'.format(old_file, new_file))
